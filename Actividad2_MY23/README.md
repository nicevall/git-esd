# Evaluador de Expresiones Matemáticas con Pila en Python

Este proyecto implementa una Pila en Python para crear un evaluador de expresiones matemáticas simples utilizando la notación polaca inversa (NPI).

## Descripción del TDA (Tipo de Dato Abstracto) - Pila

Una Pila es una estructura de datos lineal que sigue el principio de "último en entrar, primero en salir" (LIFO, por sus siglas en inglés). El TDA de Pila implementado en este proyecto incluye las siguientes operaciones básicas:
- `push(item)`: Agrega un elemento al tope de la pila.
- `pop()`: Elimina y devuelve el elemento del tope de la pila.
- `peek()`: Devuelve el elemento del tope de la pila sin eliminarlo.
- `is_empty()`: Devuelve True si la pila está vacía, False de lo contrario.
- `size()`: Devuelve el número de elementos en la pila.

## Descripción de la Aplicación

La aplicación desarrollada utiliza la Pila implementada para evaluar expresiones matemáticas simples en notación polaca inversa. Soporta operaciones básicas como suma (+), resta (-), multiplicación (*) y división (/). 

## Instrucciones de Uso

Para compilar y ejecutar la aplicación, sigue estos pasos:

1. Asegúrate de tener Python instalado en tu sistema.
2. Descarga o clona este repositorio en tu máquina local.
3. Abre una terminal y navega al directorio donde se encuentra el código.
4. Ejecuta el siguiente comando para iniciar la aplicación:

```bash
python notacion_pol.py


