class Stack:
    def __init__(self):
        # Inicialización de la pila con una lista vacía
        self.items = []

    def is_empty(self):
        # Método para verificar si la pila está vacía
        return self.items == []

    def push(self, item):
        # Método para agregar un elemento a la pila
        self.items.append(item)

    def pop(self):
        # Método para eliminar y devolver el elemento superior de la pila
        if not self.is_empty():
            return self.items.pop()
        else:
            return None

    def peek(self):
        # Método para obtener el elemento superior de la pila sin eliminarlo
        if not self.is_empty():
            return self.items[-1]
        else:
            return None

    def size(self):
        # Método para obtener el tamaño de la pila
        return len(self.items)


def evaluate_expression(expression):
    # Función para evaluar una expresión aritmética en notación posfija
    stack = Stack()  # Creamos una pila vacía
    operators = {'+': lambda x, y: x + y,  # Diccionario de operadores y sus funciones correspondientes
                 '-': lambda x, y: x - y,
                 '*': lambda x, y: x * y,
                 '/': lambda x, y: x / y}

    for token in expression.split():  # Iteramos sobre cada token en la expresión
        if token.isdigit():
            # Si el token es un número, lo convertimos a entero y lo agregamos a la pila
            stack.push(int(token))
        elif token in operators:
            # Si el token es un operador, verificamos si hay suficientes operandos en la pila
            if stack.size() < 2:
                print("Expresión inválida")
                return None
            else:
                # Obtenemos los dos operandos superiores de la pila
                operand2 = stack.pop()
                operand1 = stack.pop()
                # Realizamos la operación correspondiente y agregamos el resultado a la pila
                result = operators[token](operand1, operand2)
                stack.push(result)
        else:
            # Si el token no es ni un número ni un operador reconocido, imprimimos un mensaje de error
            print("Token no reconocido:", token)
            return None

    if stack.size() == 1:
        # Si queda exactamente un elemento en la pila, es el resultado de la expresión
        return stack.pop()
    else:
        # Si hay más o menos de un elemento en la pila, la expresión es inválida
        print("Expresión inválida")
        return None

# Ejemplo de uso
expresion = "3 4 + 5 *"  # Definimos una expresión en notación posfija
resultado = evaluate_expression(expresion)  # Evaluamos la expresión
if resultado is not None:
    # Si el resultado no es None, imprimimos el resultado
    print("El resultado de la expresión", expresion, "es:", resultado)

