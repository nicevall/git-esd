def bubble_sort(arreglo):
    n = len(arreglo)
    for i in range(n):
        for j in range(0, n-i-1):
            if arreglo[j] > arreglo[j+1]:
                arreglo[j], arreglo[j+1] = arreglo[j+1], arreglo[j]

def selection_sort(arreglo):
    n = len(arreglo)
    for i in range(n):
        min_index = i
        for j in range(i+1, n):
            if arreglo[j] < arreglo[min_index]:
                min_index = j
        arreglo[i], arreglo[min_index] = arreglo[min_index], arreglo[i]

arreglo = [64, 25, 12, 22, 11]
print("Arreglo original:", arreglo)

bubble_sort(arreglo.copy())
print("Arreglo ordenado con Bubble Sort:", arreglo)

selection_sort(arreglo.copy())
print("Arreglo ordenado con Selection Sort:", arreglo)



