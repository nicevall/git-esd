# Proyecto de Ordenamiento de Arreglos

## Descripción

Este proyecto implementa los algoritmos de ordenamiento Bubble Sort y Selection Sort para ordenar arreglos de números enteros. 

## Problemas Abordados

El proyecto aborda el problema de ordenar un arreglo de números enteros en orden ascendente.

## Algoritmos Implementados

El proyecto incluye implementaciones de los siguientes algoritmos de ordenamiento:
- Bubble Sort
- Selection Sort

## Instrucciones de Ejecución

Para ejecutar el código, sigue estos pasos:
1. Clona el repositorio desde GitHub.
2. Abre el archivo `algo_arreglos.py` en tu entorno de desarrollo Python.
3. Ejecuta el script `algo_arreglos.py`.

## Ejemplos de Casos de Prueba y Salidas

A continuación, se proporciona un ejemplo de ejecución del código con un arreglo de entrada y las salidas ordenadas por cada algoritmo de ordenamiento:

```python
arreglo = [64, 25, 12, 22, 11]
print("Arreglo original:", arreglo)

bubble_sort(arreglo.copy())
print("Arreglo ordenado con Bubble Sort:", arreglo)

selection_sort(arreglo.copy())
print("Arreglo ordenado con Selection Sort:", arreglo)


