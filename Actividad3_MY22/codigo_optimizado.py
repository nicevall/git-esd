import random
import multiprocessing

# Genera una lista de números aleatorios utilizando un generador
def generar_lista(tamano):
    return (random.randint(1, 100) for _ in range(tamano))

# Encuentra el elemento máximo en la lista
def encontrar_maximo(lista):
    return max(lista)

# Suma todos los elementos en la lista utilizando multiprocessing
def sumar_elementos(lista):
    return sum(lista)

# Main
if __name__ == "__main__":
    tamano_lista = 1000
    lista = list(generar_lista(tamano_lista))
    
    # Procesamiento en paralelo para encontrar el maximo y la suma
    with multiprocessing.Pool(2) as pool:
        maximo_result = pool.apply_async(encontrar_maximo, [lista])
        suma_result = pool.apply_async(sumar_elementos, [lista])

        maximo = maximo_result.get()
        suma = suma_result.get()

    print(f"Lista: {lista}")
    print(f"Máximo: {maximo}")
    print(f"Suma: {suma}")
