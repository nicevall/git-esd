import random
import time
import multiprocessing

# Código original

def generar_lista_original(tamano):
    return [random.randint(1, 100) for _ in range(tamano)]

def encontrar_maximo_original(lista):
    maximo = lista[0]
    for num in lista:
        if num > maximo:
            maximo = num
    return maximo

def sumar_elementos_original(lista):
    suma = 0
    for num in lista:
        suma += num
    return suma

# Codigo optimizado

def generar_lista_optimizada(tamano):
    return (random.randint(1, 100) for _ in range(tamano))

def encontrar_maximo_optimizado(lista):
    return max(lista)

def sumar_elementos_optimizado(lista):
    return sum(lista)

# Main para correr los tests
def run_tests():
    sizes = [1000, 10000, 100000, 1000000]
    
    print("Original Code:")
    for size in sizes:
        start_time = time.time()
        lista = generar_lista_original(size)
        maximo = encontrar_maximo_original(lista)
        suma = sumar_elementos_original(lista)
        end_time = time.time()
        print(f"Size: {size}, Time: {end_time - start_time:.6f} seconds")

    print("\nOptimized Code:")
    for size in sizes:
        start_time = time.time()
        lista = list(generar_lista_optimizada(size))
        maximo = encontrar_maximo_optimizado(lista)
        suma = sumar_elementos_optimizado(lista)
        end_time = time.time()
        print(f"Size: {size}, Time: {end_time - start_time:.6f} seconds")

if __name__ == "__main__":
    run_tests()
